package com.example.f

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment


class BlankFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_blank, container, false)

        val editText: EditText = rootView.findViewById(R.id.editTextText)
        val btn: Button = rootView.findViewById(R.id.button3)
        val shareBtn: Button = rootView.findViewById(R.id.button4)

        btn.setOnClickListener {
            val textToShare = editText.text.toString()

            if (textToShare.isNotEmpty()) {
                Toast.makeText(requireContext(), textToShare, Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(requireContext(), "Введите текст для отправки", Toast.LENGTH_SHORT).show()
            }
        }

        shareBtn.setOnClickListener {
            val textToShare = editText.text.toString()

            if (textToShare.isNotEmpty()) {
                shareText(textToShare)
            } else {
                Toast.makeText(requireContext(), "Введите текст для отправки", Toast.LENGTH_SHORT).show()
            }
        }

        return rootView
    }

    private fun shareText(text: String) {
        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, text)
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject")

        startActivity(Intent.createChooser(shareIntent, null))
    }
    companion object {
        @JvmStatic
        fun newInstance(): BlankFragment {
            return BlankFragment()
        }
    }
}